# This file is part account_invoice_prevent_duplicates module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool
from trytond.modules.account_invoice.exceptions import InvoiceSimilarWarning
from .exceptions import InvoiceSimilarError


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        # Reference should be required to detect duplicates
        if 'type' not in cls.reference.depends:
            old_required = cls.reference.states.get('required', Bool(False))
            cls.reference.states.update({
                    'required': old_required | ((Eval('type') == 'in')
                        & ~Eval('state').in_(['draft', 'cancelled']))
                    })

    @classmethod
    def _check_similar(cls, invoices, type='in'):
        try:
            super()._check_similar(invoices, type)
        except InvoiceSimilarWarning as e:
            raise InvoiceSimilarError(e.message)
